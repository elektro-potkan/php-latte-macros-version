VersionMacros for Latte
=======================

Macros simplifying assets reloading for Latte templating engine.
Generates version or git commit hash to be appended to the assets url effectivelly forcing the browsers to reload them.

There are also macros to get just the version string of a package or the project itself.

**Note:**
Composer v1 support is dropped from v2 onwards.
Use the v1 version of this package to support Composer versions 1 and 2.


Usage
-----
### Registering
First, register the macros in Latte, preferably using NEON configuration file:
```
latte:
	macros:
		- @ElektroPotkan\LatteMacrosVersion\VersionMacros

services:
	- ElektroPotkan\LatteMacrosVersion\VersionMacros
	- ElektroPotkan\ProjectVersion\ProjectVersionGit
```

Or directly in PHP:
```php
$latte = new Latte\Engine;

$projectVersion = new ElektroPotkan\ProjectVersion\ProjectVersionGit;
$versionMacros = new ElektroPotkan\LatteMacrosVersion\VersionMacros($projectVersion);

$versionMacros->install($latte->getCompiler());
```

You could also use simpler project-version stub `ProjectVersionStatic` or any other class implementing `IProjectVersion`.

### Generating URLs
When registered, You can use the macro inside the template like this:
```html
<link rel="stylesheet" href="path-to-some-library-css/example.css?{VER vendor/example-project}" media="all" />
<link rel="stylesheet" href="css/style.css?{VER}" media="all" />
```

This will generate the following code, provided,
that the normalized version of `vendor/example-project` composer package is `4.2.18.0`
and the project itself does not have tags and is at commit `9d23937`.
```html
<link rel="stylesheet" href="path-to-some-library-css/example.css?_ver=4.2.18.0" media="all" />
<link rel="stylesheet" href="css/style.css?_ver=9d23937" media="all" />
```

### Version strings
You can use the `VERSION` macro inside the template to get just the version strings, e.g.:
```html
Library version: {VERSION vendor/example-project}<br>
Project version: {VERSION}
```
will generate:
```html
Library version: 4.2.18.0<br>
Project version: 9d23937
```
for the same versions of packages as in the example above.


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
