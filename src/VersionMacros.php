<?php

declare(strict_types=1);

namespace ElektroPotkan\LatteMacrosVersion;

use Composer\InstalledVersions;
use ElektroPotkan\ProjectVersion\IProjectVersion;
use Latte;
use Nette;


/**
 * Latte macros to get version of Composer packages and procject itself
 *
 * Macros for assets reloading - appends version or git commit hash to the url effectivelly forcing the browsers to reload them:
 * - {VER}
 * - {VER package}
 *
 * Macros returning the version string:
 * - {VERSION}
 * - {VERSION package}
 */
class VersionMacros {
	use Nette\SmartObject;
	
	
	/** @var IProjectVersion */
	private $projectVersion = null;
	
	
	/**
	 * Constructor
	 */
	public function __construct(IProjectVersion $projectVersion){
		$this->projectVersion = $projectVersion;
	} // constructor
	
	/**
	 * Installs macros into Latte
	 */
	public function install(Latte\Compiler $compiler): void {
		$set = new Latte\Macros\MacroSet($compiler);
		$set->addMacro('VER', [$this, 'macroVER']);
		$set->addMacro('VERSION', [$this, 'macroVERSION']);
	} // install
	
	/**
	 * Processes macros {VER} and {VER package}
	 */
	public function macroVER(Latte\MacroNode $node, Latte\PhpWriter $writer): string {
		return ' ?>_ver=' . $this->getVersion($node) . '<?php ';
	} // macroVER
	
	/**
	 * Processes macros {VERSION} and {VERSION package}
	 */
	public function macroVERSION(Latte\MacroNode $node, Latte\PhpWriter $writer): string {
		return ' ?>' . $this->getVersion($node) . '<?php ';
	} // macroVERSION
	
	/**
	 * Returns version for the macro* methods
	 */
	private function getVersion(Latte\MacroNode $node): string {
		$package = $node->args;
		
		$version = null;
		
		if($package !== ''){
			$version = InstalledVersions::getVersion($package);
		};
		
		// also serves as a fallback when the package is provided/replaced
		if($version === null){
			$version = $this->projectVersion->getVersion();
		};
		
		return $version;
	} // getVersion
} // class VersionMacros
